/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;

public class TopBanner extends JPanel {
    private JLabel topBanner;

    public TopBanner() {
        topBanner = new JLabel("Customize your order and press calculate");
        add(topBanner);
    }
}