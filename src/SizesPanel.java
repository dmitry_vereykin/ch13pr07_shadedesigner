/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;

public class SizesPanel extends JPanel {
    public final double INCHES25 = 0;
    public final double INCHES27 = 2;
    public final double INCHES32 = 4;
    public final double INCHES40 = 6;

    private JRadioButton inches25;
    private JRadioButton inches27;
    private JRadioButton inches32;
    private JRadioButton inches40;
    private ButtonGroup bg;

    public SizesPanel() {
        setLayout(new GridLayout(4, 1));
   
        inches25 = new JRadioButton("25 inches wide", true);
        inches27 = new JRadioButton("27 inches wide");
        inches32 = new JRadioButton("32 inches wide");
        inches40 = new JRadioButton("40 inches wide");

        bg = new ButtonGroup();
        bg.add(inches25);
        bg.add(inches27);
        bg.add(inches32);
        bg.add(inches40);

        setBorder(BorderFactory.createTitledBorder("Sizes"));

        add(inches25);
        add(inches27);
        add(inches32);
        add(inches40);
    }

    public double getSizeCost() {
        double sizeCost = 0.0;

        if (inches25.isSelected())
            sizeCost = INCHES25;
        else if (inches27.isSelected())
            sizeCost = INCHES27;
        else if (inches32.isSelected())
            sizeCost = INCHES32;
        else if (inches40.isSelected())
            sizeCost = INCHES40;

        return sizeCost;
    }
}
