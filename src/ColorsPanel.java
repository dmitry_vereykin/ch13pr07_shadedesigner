/**
 * Created by Dmitry Vereykin on 7/27/2015.
 */
import javax.swing.*;
import java.awt.*;

public class ColorsPanel extends JPanel {
    public final double NATURAL = 5;
    public final double BLUE = 0;
    public final double TEAL = 0;
    public final double RED = 0;
    public final double GREEN = 0;

    private JRadioButton natural;
    private JRadioButton blue;
    private JRadioButton teal;
    private JRadioButton red;
    private JRadioButton green;
    private ButtonGroup bg;

    public ColorsPanel() {
        setLayout(new GridLayout(5, 1));
   
        natural = new JRadioButton("Natural ", true);
        blue = new JRadioButton("Blue");
        teal = new JRadioButton("Teal");
        red = new JRadioButton("Red");
        green = new JRadioButton("Green");

        bg = new ButtonGroup();
        bg.add(natural);
        bg.add(blue);
        bg.add(teal);
        bg.add(red);
        bg.add(green);

        setBorder(BorderFactory.createTitledBorder("Colors"));

        add(natural);
        add(blue);
        add(teal);
        add(red);
        add(green);
    }

    public double getColorCost() {
        double colorCost = 0.0;

        if (natural.isSelected())
            colorCost = NATURAL;
        else if (blue.isSelected())
            colorCost = BLUE;
        else if (teal.isSelected())
            colorCost = TEAL;
        else if (red.isSelected())
            colorCost = RED;
        else if (green.isSelected())
            colorCost = GREEN;

        return colorCost;
    }
}
